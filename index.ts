import { parse } from "./src/parser.ts";
import { take } from "./src/patterns/take.ts";
import { many } from "./src/patterns/many.ts";

const state = parse("this is a test", many(take(2)));
console.log(state);