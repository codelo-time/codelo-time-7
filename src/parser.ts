import { ParserError, createState } from "./state.ts";
import { ParserState } from "./state.ts";


export type Pattern <T = any> = {
	(state: ParserState): ParserState<T>;
}

export const parse = (content: string, pattern: Pattern): ParserState => {
	try {
		return pattern(createState(content))
	}
	catch (e) {
		if (e instanceof ParserError) {
			console.error(e.message);
			console.error(e.trace);
			console.error(e.stack);
			throw new Error('Failed to parse content.');
		} else {
			throw e;
		}
	}
}