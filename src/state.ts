/**
 * Stores state of the parser as it goes.
 * @property {number} index The pointer for what we have parsed so far.
 * @property {string} content the content we are parsing.
 * @property {any[]} results the tokens we have parsed so far.
 */
export type ParserState <T = any> = {
	index: number;
	content: string;
	results: T[];
}

/**
 * Creates a fresh state for the content.
 * @param content the content to parse.
 */
export const createState = (content: string): ParserState => ({
	index: 0,
	results: [],
	content,
})

/**
 * Adds a new result to the state and updates the index.
 * @param state what state to base the new state off.
 * @param result the result to append.
 * @param index the new index.
 */
export const appendState = <T = any>(state: ParserState<T>, result: T, index: number): ParserState<T> => ({
	...state,
	index,
	results: [...state.results, result],
})

/**
 * Replaces the whole result and updates the index.
 * @param state what state to base the new state off.
 * @param results the new results list.
 * @param index the new index.
 */
export const replaceState = <T = any>(state: ParserState<any>, results: T[], index: number): ParserState<T> => ({
	...state,
	index,
	results,
})

/**
 * Allows to reset the results and progress the index.
 * @param state what state to base the new state off.
 * @param index the new index.
 */
export const emptyState = <T = any>(state: ParserState<any>, index: number): ParserState<T> => ({
	...state,
	results: [],
	index,
})


/**
 * Stores and represents the error during parsing.
 */
export class ParserError extends Error {
	state: ParserState;

	constructor(state: ParserState, message: string) {
		super(message);
		this.state = state;
	}

	get trace(): string {
		return Deno.inspect(this.state, {
			colors: true,
			depth: Infinity,
		});
	}
}