import { Pattern } from '../parser.ts'
import { ParserError } from '../state.ts'

export const many = (pattern: Pattern, min = 1, max = Infinity): Pattern => state => {
	let currState = state;

	try {
		while (currState.index < currState.content.length + 1 && currState.results.length < max) {
			currState = pattern(currState);
		}
	}
	catch {
		if (currState.results.length < min) {
			throw new ParserError(state, `Expected ${min} instances of pattern, only got ${currState.results.length}`);
		}
	}

	return currState;
}