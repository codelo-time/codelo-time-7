import { Pattern } from '../parser.ts'
import { ParserError, appendState } from '../state.ts';

export const take = (count: number): Pattern<string> => state => {
	if (state.index + count > state.content.length) {
		throw new ParserError(state, 'Unexpected end of input');
	}

	const result = state.content.substring(state.index, state.index + count);
	return appendState(state, result, state.index + count);
}